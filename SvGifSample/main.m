/************************************************************
 *  * HongHe CONFIDENTIAL
 * __________________
 * Copyright (C) 2013-2014 HongHe Technologies. All rights reserved.
 *
 * NOTICE: All information contained herein is, and remains
 * the property of HongHe Technologies.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from HongHe Technologies.
 *
 * project created by knight 349919133
 *
 */

#import <UIKit/UIKit.h>

#import "SvGifAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SvGifAppDelegate class]));
    }
}
